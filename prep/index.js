const fs = require("fs").promises;
const { asFileName } = require("./fileChunks");
const Minio = require("minio");

const {
  MINIO_HOST,
  MINIO_PORT,
  MINIO_SSL_ENABLED = false,
  MINIO_ACCESS_KEY,
  MINIO_SECRET_KEY,
  POLLING_INTERVAL = 1000,
  TEMP_FOLDER_PATH = "../server-go/temp/"
} = process.env;

// Instantiate the minio client with the endpoint
// and access keys as shown below.
const minioClient = new Minio.Client({
  endPoint: MINIO_HOST,
  port: MINIO_PORT,
  useSSL: MINIO_SSL_ENABLED,
  accessKey: MINIO_ACCESS_KEY,
  secretKey: MINIO_SECRET_KEY
});

const PATH_PACKET_INDEX = 0;

setInterval(pollFiles, POLLING_INTERVAL);

async function pollFiles() {
  const files = (await fs.readdir(TEMP_FOLDER_PATH))
    .map(fileName => fileName.split("_"))
    .reduce((acc, [sha1, total, index]) => {
      acc = { [sha1]: { total, existing: [] }, ...acc };
      acc[sha1].existing.push(index);
      return acc;
    }, {});

  return handlePartialFiles(files);
}

function handlePartialFiles(files) {
  const filesHandled = [];
  let sha1;

  for (sha1 in files) {
    const { total, existing } = files[sha1];
    if (+total !== existing.length) {
      filesHandled.push(handleFullFile(sha1, total));
    } else {
      // console.log("File ____ is yet to be fully recieved....")
    }
  }

  return await Promise.all(filesHandled);
}

async function handleFullFile([sha1, { total }]) {
  const unparsedDestination = await fs.open(
    asFileName(sha1, total, PATH_PACKET_INDEX)
  );
  const [bucket, destination] = unparsedDestination
    .readFile({ encoding: "utf-8" })
    .split(";", 1);
  const fullFileStream = streamFilesToMinio(TEMP_FOLDER_PATH, sha1, total);
  minioClient.fPutObject(bucket, destination, fullFileStream);
}
