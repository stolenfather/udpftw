module.exports = {
  asFileName
};

function asFileName(sha1, total, index) {
  return `${sha1}_${total}_${index}`;
}
