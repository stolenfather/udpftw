const MultiStream = require("multistream");
const fs = require("fs");
const { asFileName } = require("./fileChunks");

module.exports = streamFilesToMinio;

function streamFilesToMinio(path, sha1, total) {
  const streams = [];

  for (let i = 1; i < total; i++) {
    streams.push(fs.createReadStream(`${path}/${asFileName(sha1, total, i)}`));
  }

  return new MultiStream(streams);
}
