const fs = require("fs").promises;
const dgram = require("dgram");
const server = dgram.createSocket("udp4");

const PORT = 33333;
const HOST = "localhost";
const FILE_LOCATION_PREFIX = "./temp";

server.on("listening", function() {
  var address = server.address();
  console.log(`UDP Server listening on ${address.address}:${address.port}`);
});

server.on("message", handleMessage);

let i = 0;

async function handleMessage(message, remote) {
  /**
   * message: Buffer
   * remote: Object.of({address: string, family: string, port: number, size: number})
   */
  setImmediate(function () {
    i++;
    const index = message.readUInt32BE(0);
    const count = message.readUInt32BE(4);
    const sha1 = message.slice(8, 28).toString("hex");
    writeToFile(
      `${FILE_LOCATION_PREFIX}/${sha1}_${count}_${index}`,
      message,
      28
    );
  });
}

async function writeToFile(fileName, message, offset) {
  try {
    const fileHandler = await fs.open(fileName, "ax+");
    await fileHandler.write(message, offset, message.length - offset);
    await fileHandler.close();
    console.log(`Written: ${fileName}`);
  } catch (e) {
    if (e.code !== "EEXIST") {
      throw e;
    }
  }
}

server.bind(PORT, HOST);

setTimeout(() => console.log("Packets Recieved in the last 5 seconds: %d", i), 5000);
