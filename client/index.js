const express = require("express");
const expressPrometheusMiddleware = require("express-prom-bundle");
const bodyParser = require("body-parser");
const fileQueue = require("./file-queue");
const delays = require("./delays");

require("dotenv").config();

const { DESTINATION_HOST, DESTINATION_PORT, PORT = 3000 } = process.env;

const app = express();

app.use(bodyParser.json());
app.use(expressPrometheusMiddleware({ includeMethod: true }));

app.post(
  "/stream",
  ({ body: { destination, filepath, filesha1 }, ...req }, res) => {
    delays
      .map((delay, attempt) =>
        fileQueue.queue.add(
          {
            destination,
            filepath,
            filesha1,
            destHost: DESTINATION_HOST,
            destPort: DESTINATION_PORT,
            attempt
          },
          { delay }
        )
      )
      .reduce(Promise.all, Promise.resolve())
      .then(() => res.sendStatus(200));
  }
);

app.listen(PORT, () => console.log(`App listening on port ${PORT}!`));
