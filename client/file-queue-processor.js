const streamFile = require("./streamFile");

module.exports = function(job, {destHost, destPort, filepath, filesha1, destination, attempt}) {
  return streamFile.overUDP({
    host: destHost,
    port: destPort,
    filepath,
    filesha1,
    destination
  });
};
