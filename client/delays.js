const NO_DELAY = undefined;
const HALF_A_MINUTE = 1000 * 30;
const A_MINUTE = HALF_A_MINUTE * 2;

const delays = [NO_DELAY, HALF_A_MINUTE, A_MINUTE];

module.exports = {
  delays
};
