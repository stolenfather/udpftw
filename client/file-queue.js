const os = require("os");
const Bull = require("bull");

const { REDIS_HOST, REDIS_PORT } = process.env;

const CPU_COUNT = os.cpus().length;

const fileSendingQueue = new Bull("file send", {
  redis: { host: REDIS_HOST, port: REDIS_PORT }
});

fileSendingQueue.process(CPU_COUNT, "./file-queue-processor.js");

module.exports = {
  queue
};